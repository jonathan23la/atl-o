var Publicidade = {
    initialize: function(){},
    initForm: function(){
        $j('.containerForm form').validate({
                rules: {
                    "publicidade.titulo": {
                        required: true,
                        minlength: 3
                    },
                    "publicidade.link": {
                        required: true,
                        url: true
                    },
                    "publicidade.produto": {
                        required: true
                    },
                    "publicidade.estabelecimento": {
                        required: true
                    }
                }
            });
            Publicidade.selectTipoLink($j('#tipoLink').val());
    },
    
    selectTipoLink: function(value){
        $j('.tipoLink').hide();
        if (value !== "")
        {
            $j('.tipoLink'+value).show();
        }
    }
};