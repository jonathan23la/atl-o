var Ajax = 
{
    loadingImg: '<div class="loadingImg" align="center" style="padding: 5px;"><img alt="" src="../sistema/images/loader.gif"></div>',
    confirmImg: '<div class="confirmImg" align="center" style="padding: 5px;"><img alt="" src="../sistema/images/confirm.png"></div>',
    errorImg: '<div class="errorImg" align="center" style="padding: 5px;"><img alt="" src="../sistema/images/error.png"></div>',
    
    execute: function(param)
    {
        param.beforeSend = param.beforeSend || function(){};
        param.success = param.success || function(){};
        param.url = param.url || "";
        param.data = param.data || "";
        
        if (param.url == "" || param.data == "")
        {
            alert("Atenção! A url e os paramtros são obrigatorios!"); 
        }
        else
        {
            param.beforeSend();
            $j.ajax({
                url: param.url,
                type: "POST",
                data: param.data,
                success: function(html){
                    param.success(html)
                }
            });
        }    
    }    
}