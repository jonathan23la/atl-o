<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Membro">
    <ovc:containerForm titulo="Editar Membro" action="altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Nome:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="membro.nome" value="${membro.nome}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nome">Horas:</label>
                </td>
                <td>
                    <input id="horas" type="text" name="membro.horas" value="${membro.horas}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="funcao">Funcao:</label>
                </td> 
                <td>
                    <select id="funcao" name="membro.funcao" style="width: 200px" class="inputDefault">
                        <option value="">Selecione Funcao</option>
                        <c:forEach items="${funcaoList}" var="funcao">
                            <option value="${funcao}" <c:if test="${funcao == membro.funcao}">selected</c:if> >${funcao.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="membro.id" value="${membro.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
    <script type="text/javascript">
        $j(document).ready(function()
        {
            $j('.containerForm form').validate({
                //errorClass: 'ui-state-error',
                rules: {
                    "membro.nome": {
                        required: true,
                        minlength: 3
                    }
                }
            });
        });
    </script>
</ovc:main>