<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Historias">
    <ovc:containerList lista="${historiaList}" titulo="Lista de Historias" vazio="Nenhuma historia encontrada!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Titiulo</th>
                    <th>Pontos</th>
                    <th>Prioridade</th>
                    <th>Tarefas</th>
                    <th colspan="2">Tarefas</th>
                    <th colspan="1">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${historiaList}" var="historia">
                    <tr>
                        <td class="tdData">${historia.titulo}</td>
                        <td class="tdData">${historia.pontos}</td>
                        <td class="tdData">${historia.prioridade}</td>
                        <td class="tdData">${historia.numTarefas}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-search" href="<c:url value="/historia/${historia.id}/tarefa/lista" />">Lista de Tarefas</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-plusthick" href="<c:url value="/historia/${historia.id}/tarefa/formulario" />">Nova Tarefa</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita/${historia.id}">Editar</a>
                        </td>
                        <!--td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove/${historia.id}">Remover</a>
                        </td-->
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>