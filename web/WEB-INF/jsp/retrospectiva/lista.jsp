<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Item de Retrospectiva">
    <ovc:containerList lista="${itemRetrospectivaList}" titulo="Lista de Item de Retrospectiva" vazio="Nenhum item de retrospectiva encontrado!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Tipo</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${itemRetrospectivaList}" var="itemRetrospectiva">
                    <tr>
                        <td class="tdData">${itemRetrospectiva.descricao}</td>
                        <td class="tdData">${itemRetrospectiva.tipo.descricao}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita?id=${itemRetrospectiva.id}">Editar</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove?id=${itemRetrospectiva.id}">Remover</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>