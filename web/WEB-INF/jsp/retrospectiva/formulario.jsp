<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Projeto" title="Adicionar Projeto">
    <ovc:containerForm titulo="Adicionar Projeto" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="nome">Descrição:</label>
                </td>
                <td>
                    <input id="nome" type="text" name="itemRetrospectiva.descricao" value="${itemRetrospectiva.descricao}" style="width: 500px" class="inputDefault"/>
                </td>
                <tr>
                    <td align="right">
                        <label for="perfil">Tipo:</label>
                    </td> 
                    <td>
                        <select id="codPerfil" name="itemRetrospectiva.tipo" style="width: 200px" class="inputDefault">
                            <option value="">Selecione Tipo</option>
                            <c:forEach items="${tipoItemRetrospectivaList}" var="tipo">
                                <option value="${tipo}" >${tipo.descricao}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
