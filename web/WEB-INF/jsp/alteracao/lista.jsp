<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Alteracaos">
    <ovc:containerList lista="${alteracaoList}" titulo="Controle de Alterações" vazio="Nenhuma alteração encontrada!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Tabela / Arquivo</th>
                    <th>Local / Pasta / Banco</th>
                    <th>Tipo</th>
                    <th>Versionado</th>
                    <th>Maquina de Teste</th>
                    <th>Observação</th>
                    <th>Site</th>
                    <th colspan="3">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${alteracaoList}" var="alteracao">
                    <tr>
                        <td class="tdData">${alteracao.nome}</td>
                        <td class="tdData">${alteracao.referencia}</td>
                        <td class="tdData">${alteracao.tipo.descricao}</td>
                        <td class="tdData">
                            <c:choose>
                                <c:when test="${alteracao.versionado}">Sim</c:when>
                                <c:otherwise>Não</c:otherwise>
                            </c:choose>
                        </td>
                        <td class="tdData">
                            <c:choose>
                                <c:when test="${alteracao.maquinaTeste}">Sim</c:when>
                                <c:otherwise>Não</c:otherwise>
                            </c:choose>
                        </td>
                        <td class="tdData">${alteracao.observacao}</td>
                        <td class="tdData">
                            <c:choose>
                                <c:when test="${alteracao.envioSite}">
                                    <img alt="Sim" src="<c:url value="/sistema/images/confirm.png"/>">
                                </c:when>
                                <c:otherwise>
                                    <img alt="Não" src="<c:url value="/sistema/images/error.png"/>">
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon ui-icon-arrowthickstop-1-n" href="envioSite/${alteracao.id}">Enviar Site</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita/${alteracao.id}">Editar</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove/${alteracao.id}">Remover</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="toolbar" align="center">
            <button type="reset" icon="ui-icon-plusthick" onclick='window.location.href="formulario";'>Nova Alteração</button>
        </div>
    </ovc:containerList>
</ovc:main>