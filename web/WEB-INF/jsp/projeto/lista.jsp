<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Projetos">
    <ovc:containerList lista="${projetoList}" titulo="Lista de Projetos" vazio="Nenhuma projeto encontrada!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Nome</th>
                    <th colspan="2">Historia</th>
                    <th colspan="2">Sprint</th>
                    <th colspan="1">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${projetoList}" var="projeto">
                    <tr>
                        <td class="tdData">${projeto.nome}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-search" href="<c:url value="/projeto/${projeto.id}/historia/lista" />">Lista de Historia</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-plusthick" href="<c:url value="/projeto/${projeto.id}/historia/formulario" />">Nova Historia</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-search" href="<c:url value="/projeto/${projeto.id}/sprint/lista" />">Lista de Sprints</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-plusthick" href="<c:url value="/projeto/${projeto.id}/sprint/formulario" />">Nova Sprint</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita?id=${projeto.id}">Editar</a>
                        </td>
                        <!--td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove?id=${projeto.id}">Remover</a>
                        </td-->
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>