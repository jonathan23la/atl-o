<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Tarefa">
    <ovc:containerForm titulo="Editar Tarefa" action="../altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="descricao">Descrição:</label>
                </td>
                <td>
                    <textarea id="descricao" name="tarefa.descricao" style="width: 538px; height: 200px;" class="tinymce">
                        ${tarefa.descricao}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="horas">Horas Estimadas:</label>
                </td>
                <td>
                    <input id="horas" type="text" name="tarefa.horas" value="${tarefa.horas}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            
            <tr>
                <td align="right">
                    <label for="horas">Horas Gasta:</label>
                </td>
                <td>
                    <input id="horasGasta" type="text" name="tarefa.horasGasta" value="${tarefa.horasGasta}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            
            <tr>
                <td align="right">
                    <label for="membro">Membro:</label>
                </td> 
                <td>
                    <select id="membro" name="tarefa.membro.id" style="width: 200px" class="inputDefault">
                        <option value="">Não Atribuido</option>
                        <c:forEach items="${membroList}" var="membro">
                            <option value="${membro.id}" <c:if test="${membro.id == tarefa.membro.id}">selected</c:if> >${membro.nome}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="tipoTarefa">Tipo:</label>
                </td> 
                <td>
                    <select id="tipoTarefa" name="tarefa.tipo" style="width: 200px" class="inputDefault">
                        <option value="">Selecione</option>
                        <c:forEach items="${tipoTarefaList}" var="tipo">
                            <option value="${tipo}" <c:if test="${tipo == tarefa.tipo}">selected</c:if> >${tipo.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="statusTarefa">Status:</label>
                </td> 
                <td>
                    <select id="statusTarefa" name="tarefa.status" style="width: 200px" class="inputDefault">
                        <option value="">Selecione</option>
                        <c:forEach items="${statusTarefaList}" var="status">
                            <option value="${status}" <c:if test="${status == tarefa.status}">selected</c:if> >${status.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="tarefa.id" value="${tarefa.id}" />
                    <input type="hidden" name="tarefa.historia.id" value="${tarefa.historia.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
    <script type="text/javascript">
        $j(document).ready(function()
        {
            $j('.containerForm form').validate({
                //errorClass: 'ui-state-error',
                rules: {
                    "historia.nome": {
                        required: true,
                        minlength: 3
                    }
                }
            });
        });
    </script>
</ovc:main>