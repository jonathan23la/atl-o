<%--
    Document   : formulario
    Created on : 05/10/2011, 10:37:14
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" javaScript="Tarefa" title="Adicionar Tarefa">
    <ovc:containerForm titulo="Adicionar Tarefa" action="adiciona" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="descricao">Descrição:</label>
                </td>
                <td>
                    <textarea id="descricao" name="tarefa.descricao" style="width: 538px; height: 200px;" class="tinymce">
                        ${tarefa.descricao}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="horas">Horas:</label>
                </td>
                <td>
                    <input id="horas" type="text" name="tarefa.horas" value="${tarefa.horas}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="membro">Membro:</label>
                </td> 
                <td>
                    <select id="membro" name="tarefa.membro.id" style="width: 200px" class="inputDefault">
                        <option value="">Não Atribuido</option>
                        <c:forEach items="${membroList}" var="membro">
                            <option value="${membro.id}" <c:if test="${membro.id == tarefa.membro.id}">selected</c:if> >${membro.nome}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="tipoTarefa">Tipo:</label>
                </td> 
                <td>
                    <select id="tipoTarefa" name="tarefa.tipo" style="width: 200px" class="inputDefault">
                        <c:forEach items="${tipoTarefaList}" var="tipo">
                            <option value="${tipo}" <c:if test="${tipo == tarefa.tipo}">selected</c:if> >${tipo.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="statusTarefa">Status:</label>
                </td> 
                <td>
                    <select id="statusTarefa" name="tarefa.status" style="width: 200px" class="inputDefault">
                        <c:forEach items="${statusTarefaList}" var="status">
                            <option value="${status}" <c:if test="${status == tarefa.status}">selected</c:if> >${status.descricao}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="tarefa.historia.id" value="${historia.id}" />
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
