<%--
    Document   : edita
    Created on : 05/10/2011, 11:20:04
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Editar Sprint" javaScript="Sprint" >
    <ovc:containerForm titulo="Editar Sprint" action="../altera">
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="numero">Numero:</label>
                </td>
                <td>
                    <input id="numero" type="text" name="sprint.numero" value="${sprint.numero}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="meta">Meta:</label>
                </td>
                <td>
                    <input id="meta" type="text" name="sprint.meta" value="${sprint.meta}" style="width: 500px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="pronto">Definição de pronto:</label>
                </td>
                <td>
                    <textarea id="pronto" name="sprint.pronto" style="width: 538px; height: 200px;" class="tinymce">
                        ${sprint.pronto}
                    </textarea>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="datepickerFrom">Data:</label>
                </td> 
                <td>
                    <input id="datepickerFrom" name="sprint.dataInicial" style="width: 70px;" class="inputDefault" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${sprint.dataInicial}" />" />
                    <label for="datepickerTo">a</label>
                    <input id="datepickerTo" name="sprint.dataFinal" style="width: 70px;" class="inputDefault" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${sprint.dataFinal}" />" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="hidden" name="sprint.id" value="${sprint.id}" />
                    <input type="hidden" name="sprint.projeto.id" value="${sprint.projeto.id}" />
                    <div class="toolbar">
                        <button icon="ui-icon-pencil" type="submit">Alterar</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>