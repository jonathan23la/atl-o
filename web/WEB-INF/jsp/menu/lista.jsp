<%-- 
    Document   : lista
    Created on : 04/10/2011, 17:11:36
    Author     : Maison Chaves
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main menu="${true}" title="Lista de Itens de Menu">
    <ovc:containerList lista="${itemMenuList}" titulo="Lista de Itens de Menu" vazio="Nenhuma item de menu encontrado!" page="${page}" total="${total}" pageSize="${pageSize}">
        <table cellspacing="0" cellpadding="0" style="width: 100%" class="data">            
            <thead>
                <tr>
                    <th>Nome</th>
                    <th colspan="2">Ações</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${itemMenuList}" var="menu">
                    <tr>
                        <td class="tdData">${menu.nome}</td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-pencil" href="edita?id=${menu.id}">Editar</a>
                        </td>
                        <td width="25px" align="center" class="actionContainer">
                            <a class="actionList" icon="ui-icon-trash" href="remove?id=${menu.id}">Remover</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </ovc:containerList>
</ovc:main>