<%--
    Document   : loginForm
    Created on : 25/10/2011, 10:08:33
    Author     : Maison Chaves
--%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="ovc"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<ovc:main title="Trocar Senha" menu="${true}">
    <style type="text/css">
        .loginForm{
            width       : 450px;
            padding-top : 100px;
        }
    </style>
    <ovc:containerForm titulo="Trocar Senha" action="trocar" >
        <table style="width: 100%">
            <tr>
                <td align="right">
                    <label for="atual">Atual:</label>
                </td>
                <td>
                    <input id="atual" type="password" name="atual" style="width: 300px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="nova">Nova:</label>
                </td>
                <td>
                    <input id="nova" type="password" name="nova" style="width: 300px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <label for="confirmacao">Confirmação:</label>
                </td>
                <td>
                    <input id="confirmacao" type="password" name="confirmacao" style="width: 300px" class="inputDefault"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <div class="toolbar">
                        <button type="reset" icon="ui-icon-cancel" >Cancelar</button>
                        <button type="submit" icon="ui-icon-plus" >Inserir</button>
                    </div>
                </td>
            </tr>
        </table>
    </ovc:containerForm>
</ovc:main>
