<%-- 
    Document   : main
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="titulo" type="java.lang.String" required="true"%>
<%@attribute name="action" type="java.lang.String" required="true"%>
<%@attribute name="enctype" type="java.lang.String" %>
<%@attribute name="metodo" type="java.lang.String" %>
<jsp:doBody var="body" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ovc" tagdir="/WEB-INF/tags/" %>

<ovc:container nome="containerForm" titulo="${titulo}" >
    <form <c:if test='${metodo != null && metodo != ""}'>method="${metodo}"</c:if> action="${action}" <c:if test='${enctype != null && enctype != ""}'>enctype="${enctype}"</c:if> >
        ${body}
    </form>
</ovc:container>

