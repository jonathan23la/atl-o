<%--
    Document   : main
    Created on : 16/01/2012, 08:31:37
    Author     : Maisn Chaves
--%>

<%@tag description="put the tag description here" pageEncoding="UTF-8"%>

<%@attribute name="projetoId" type="java.lang.Integer" required="true"%>
<%@attribute name="sprintId" type="java.lang.Integer" required="true"%>
<%@attribute name="nome" type="java.lang.String" required="true"%>
<jsp:doBody var="body" />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<li>
    <a href="<c:url value="/projeto/${projetoId}/sprint/${sprintId}/historia/lista"/>">${nome}</a>
    <ul>
        ${body}
    </ul>
</li>

