
package atlas.infra;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import br.com.caelum.vraptor.ioc.ComponentFactory;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author Maison Chaves
 */
@Component
@ApplicationScoped
public class SessionFactoryCreator implements ComponentFactory<SessionFactory>
{
    private SessionFactory factory;

    /**
     * Inicia a fabrica de sessoes
     */
    @PostConstruct
    public void abre()
    {
        AnnotationConfiguration configuration = new AnnotationConfiguration();
        configuration.configure();
        this.factory = configuration.buildSessionFactory();
    }

    /**
     * Retorna a fabrica de sessao
     * @return
     */
    @Override
    public SessionFactory getInstance()
    {
        return this.factory;
    }

    /**
     * Fecha a fabrica de sessoes
     */
    @PreDestroy
    public void fecha()
    {
        this.factory.close();
    }
}