
package atlas.infra.Interceptors;

import atlas.dao.MenuDAO;
import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.caelum.vraptor.resource.ResourceMethod;

/**
 *
 * @author Maison Chaves
 */
@Intercepts
@RequestScoped
public class MenuInterceptor implements Interceptor
{
    private final MenuDAO menuDAO;
    private final Result result;

    public MenuInterceptor(MenuDAO menuDAO, Result result)
    {
        this.menuDAO = menuDAO;
        this.result = result;
    }

    @Override
    public void intercept(InterceptorStack is, ResourceMethod rm, Object o) throws InterceptionException
    {
        result.include("menuList", menuDAO.listaTudo());
    }

    @Override
    public boolean accepts(ResourceMethod rm)
    {
        return false;
    }
    
}
