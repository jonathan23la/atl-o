
package atlas.infra;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maison Chaves
 */
@Component
@ApplicationScoped
public final class Criptografia
{
    /**
     * Gera a senha criptografada
     * @param senha
     * @return
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public String criptografa(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
        byte messageDigest[] = algorithm.digest(senha.getBytes("UTF-8"));

        StringBuilder hexString = new StringBuilder();
        for (byte b : messageDigest)
        {
            hexString.append(String.format("%02X", 0xFF & b));
        }

        return hexString.toString();
    }
    /**
     * Verifica se a senha criptografada é a mesma que foi passada
     * @param senha
     * @param hash
     * @return
     */
    public Boolean checkCriptografia(String senha, String hash )
    {
        Boolean valid = false;
        try
        {
            senha = this.criptografa(senha);
            valid = senha.equals(hash);
        }
        catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(Criptografia.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            Logger.getLogger(Criptografia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valid;
    }
}
