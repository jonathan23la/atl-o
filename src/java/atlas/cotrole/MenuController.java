
package atlas.cotrole;
import atlas.dao.MenuDAO;
import atlas.modelo.ItemMenu;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class MenuController
{
    private final MenuDAO dao;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param menuDAO
     * @param result
     * @param validator
     */
    public MenuController(MenuDAO menuDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = menuDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<ItemMenu> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }
        List<ItemMenu> busca = dao.busca(page, pageSize);
        
        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param menu
     */
    public void adiciona(final ItemMenu menu)
    {
        if (menu.getPai().getId() == null)
        {
            menu.setPai(null);
        }
        
        if (menu.getNome() == null || menu.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(menu))
        {
            validator.add(new ValidationMessage("Essa menu já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(menu);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public ItemMenu edita(Long id)
    {
        return dao.carrega(id);
    }

    /**
     *
     * @param menu
     */
    public void altera(ItemMenu menu)
    {
        if (menu.getNome() == null || menu.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(menu))
        {
            validator.add(new ValidationMessage("Essa menu já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(menu.getId());

        dao.altera(menu);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        ItemMenu menu = dao.carrega(id);
        dao.remove(menu);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public List<ItemMenu> formulario()
    {
        return dao.listaTudo();
    }
    
    public List<ItemMenu> show()
    {
        return dao.listaTudo();
    }
}
