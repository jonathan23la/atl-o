
package atlas.cotrole;
import atlas.dao.MembroDAO;
import atlas.modelo.Funcao;
import atlas.modelo.Membro;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class MembroController
{
    private final MembroDAO dao;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param membroDAO
     * @param result
     * @param validator
     */
    public MembroController(MembroDAO membroDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = membroDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Membro> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param membro
     */
    public void adiciona(final Membro membro, final String confirmacao)
    {
        if (membro.getNome() == null || membro.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (membro.getLogin() == null || membro.getLogin().getSenha() == null )
        {
            validator.add(new ValidationMessage("Senha é obrigatório", "produto.nome"));
        }
        else if(!membro.getLogin().getSenha().equals(confirmacao))
        {
           validator.add(new ValidationMessage("Senhas não correspondem", "produto.nome")); 
        }
        if (!dao.check(membro))
        {
            validator.add(new ValidationMessage("Essa membro já se encontra cadastrado", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(membro);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Membro edita(Long id)
    {
        result.include("funcaoList", Arrays.asList(Funcao.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param membro
     */
    public void altera(Membro membro)
    {
        if (membro.getNome() == null || membro.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(membro))
        {
            validator.add(new ValidationMessage("Essa membro já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(membro.getId());

        dao.altera(membro);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        Membro membro = dao.carrega(id);
        dao.remove(membro);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public void formulario()
    {
         result.include("funcaoList", Arrays.asList(Funcao.values()));
    }
}
