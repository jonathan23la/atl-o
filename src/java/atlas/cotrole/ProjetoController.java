
package atlas.cotrole;
import atlas.dao.ProjetoDAO;
import atlas.modelo.Projeto;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class ProjetoController
{
    private final ProjetoDAO dao;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param projetoDAO
     * @param result
     * @param validator
     */
    public ProjetoController(ProjetoDAO projetoDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = projetoDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Projeto> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param projeto
     */
    public void adiciona(final Projeto projeto)
    {
        if (projeto.getNome() == null || projeto.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(projeto))
        {
            validator.add(new ValidationMessage("Essa projeto já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(projeto);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Projeto edita(Long id)
    {
        return dao.carrega(id);
    }

    /**
     *
     * @param projeto
     */
    public void altera(Projeto projeto)
    {
        if (projeto.getNome() == null || projeto.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(projeto))
        {
            validator.add(new ValidationMessage("Essa projeto já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(projeto.getId());

        dao.altera(projeto);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        Projeto projeto = dao.carrega(id);
        dao.remove(projeto);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public void formulario()
    {
    }
}
