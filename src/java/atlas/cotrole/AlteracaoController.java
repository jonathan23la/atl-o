package atlas.cotrole;

import atlas.dao.AlteracaoDAO;
import atlas.dao.SprintDAO;
import atlas.dao.TarefaDAO;
import atlas.modelo.Alteracao;
import atlas.modelo.Sprint;
import atlas.modelo.Tarefa;
import atlas.modelo.TipoAlteracao;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class AlteracaoController
{
    private final AlteracaoDAO dao;
    private final SprintDAO sprintDAO;
    private final Result result;
    private final Validator validator;
    private final TarefaDAO tarefaDAO;

    /**
     *
     * @param alteracaoDAO
     * @param result
     * @param validator
     */
    public AlteracaoController(AlteracaoDAO alteracaoDAO, SprintDAO sprintDAO, TarefaDAO tarefaDAO, Result result, Validator validator)
    {
        this.tarefaDAO = tarefaDAO;
        this.validator = validator;
        this.result = result;
        this.dao = alteracaoDAO;
        this.sprintDAO = sprintDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/tarefa/{tarefa.id}/alteracao/lista")
    public List<Alteracao> lista(Tarefa tarefa/*, Integer page, Integer pageSize*/)
    {
//        if (page == null || pageSize == null)
//        {
//            page = 1;
//            pageSize = 10;
//        }

        Alteracao alteracao = new Alteracao();
        alteracao.setTarefa(tarefa);

        Integer total = dao.getTotalRegistros(alteracao);

        result.include("page", 1);
        result.include("pageSize", total);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / total) + 1);
        return dao.busca(alteracao);
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/sprint/{sprintId}/controleAlteracao")
    public List<Alteracao> lista(Long sprintId)
    {
        List<Alteracao> listaAlteracoes = sprintDAO.carrega(sprintId).getAlteracoes();
        
        Integer total = listaAlteracoes.size();

        result.include("page", 1);
        result.include("pageSize", total);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / total) + 1);
        return listaAlteracoes;
    }

    /**
     *
     * @param alteracao
     */
    @Path("/tarefa/{tarefa.id}/alteracao/adiciona")
    public void adiciona(Tarefa tarefa, final Alteracao alteracao)
    {
        if (alteracao.getNome()== null || alteracao.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Titulo é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario(tarefa);

        dao.salva(alteracao);
        result.redirectTo(this).lista(tarefa);
    }

    /**
     *
     * @param id
     * @return
     */
    @Path("/tarefa/{tarefa.id}/alteracao/edita/{id}")
    public Alteracao edita(Tarefa tarefa, Long id)
    {
        result.include("tipoAlteracaoList", Arrays.asList(TipoAlteracao.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param alteracao
     */
    @Path("/tarefa/{tarefa.id}/alteracao/altera")
    public void altera(Tarefa tarefa, Alteracao alteracao)
    {
        if (alteracao.getNome()== null || alteracao.getNome().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(tarefa, alteracao.getId());

        dao.altera(alteracao);
        result.redirectTo(this).lista(tarefa);
    }

    /**
     *
     * @param id
     */
    @Path("/tarefa/{tarefa.id}/alteracao/remove/{id}")
    public void remove(Tarefa tarefa, Long id)
    {
        Alteracao alteracao = dao.carrega(id);
        dao.remove(alteracao);
        result.redirectTo(this).lista(tarefa);
    }
    
    @Path("/tarefa/{tarefa.id}/alteracao/envioSite/{id}")
    public void envioSite(Tarefa tarefa, Long id)
    {
        Alteracao alteracao = dao.carrega(id);
        alteracao.setEnvioSite(true);
        dao.altera(alteracao);
        result.redirectTo(this).lista(tarefa);
    }
    
    @Path("/sprint/{sprint.id}/envioSite/{id}")
    public void envioSite(Sprint sprint, Long id)
    {
        Alteracao alteracao = dao.carrega(id);
        alteracao.setEnvioSite(true);
        dao.altera(alteracao);
        result.redirectTo(this).lista(sprint.getId());
    }
    
    @Path("/tarefa/{tarefa.id}/alteracao/formulario")
    public Tarefa formulario(Tarefa tarefa)
    {
        result.include("tipoAlteracaoList", Arrays.asList(TipoAlteracao.values()));
        return tarefa;
    }
}
