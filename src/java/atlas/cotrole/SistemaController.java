
package atlas.cotrole;

import atlas.dao.MembroDAO;
import atlas.dao.ProjetoDAO;
import atlas.infra.Annotations.Liberado;
import atlas.infra.Criptografia;
import atlas.infra.UserSession;
import atlas.modelo.Login;
import atlas.modelo.Membro;
import atlas.modelo.Projeto;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.List;

/**
 *
 * @author Maison Chaves
 */
@Resource
public class SistemaController
{
    private ProjetoDAO projetoDAO;
    private final MembroDAO membroDAO;
    private final Result result;
    private final UserSession userSession;
    private final Validator validator;
    private final Criptografia criptografia;

    public SistemaController(ProjetoDAO projetoDAO, MembroDAO membroDAO, Result result, UserSession userSession, Validator validator, Criptografia criptografia)
    {
        this.projetoDAO = projetoDAO;
        this.membroDAO = membroDAO;
        this.result = result;
        this.userSession = userSession;
        this.validator  = validator;
        this.criptografia  = criptografia;
    }
    
    public List<Projeto> index()
    {
        return projetoDAO.listaTudo();
    }

    /**
     *
     * @param membro
     */
    @Liberado
    public void login(Login login)
    {
        Membro carregado = membroDAO.carregaLogin(login);
        List<Projeto> projetos = projetoDAO.listaTudo();

        if (carregado == null)
        {
            validator.add(new ValidationMessage("Login e/ou senha inválidos", "login.login"));
        }
        validator.onErrorRedirectTo(this).loginForm();

        userSession.logar(carregado, projetos);
        result.redirectTo(this).index();
    }

    /**
     *
     */
    @Liberado
    public void logout()
    {
        userSession.logout();
        result.redirectTo(this).loginForm();
    }

    /**
     *
     */
    @Liberado
    public void loginForm()
    {
    }
    
    /**
     *
     */
    public void trocarSenha()
    {
    }
    
    /**
     *
     */
    public void trocar(String atual, String nova, String confirmacao)
    {
        String senhaBanco = this.userSession.getMembro().getLogin().getSenha();
        if (!this.criptografia.checkCriptografia(atual, senhaBanco))
        {
            validator.add(new ValidationMessage("Senha atual não corresponde!", "login.login"));
        }
        else if(!nova.equals(confirmacao))
        {
           validator.add(new ValidationMessage("Senha nova não correspondem a confirmação!", "produto.nome")); 
        }
        validator.onErrorUsePageOf(this).trocarSenha();
        
        Login login = userSession.getMembro().getLogin();
        login.setSenha(nova);
        membroDAO.alteraSenha(login);
        result.redirectTo(this).index();
    }
}
