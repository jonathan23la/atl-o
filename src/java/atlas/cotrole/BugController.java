
package atlas.cotrole;
import atlas.dao.BugDAO;
import atlas.modelo.Funcao;
import atlas.modelo.Bug;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class BugController
{
    private final BugDAO dao;
    private final Result result;
    private final Validator validator;

    /**
     *
     * @param bugDAO
     * @param result
     * @param validator
     */
    public BugController(BugDAO bugDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = bugDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Bug> lista(Integer page, Integer pageSize)
    {
        if (page == null || pageSize == null)
        {
            page = 1;
            pageSize = 10;
        }

        Integer total = dao.getTotalRegistros();

        result.include("page", page);
        result.include("pageSize", pageSize);
        result.include("total", total);
        result.include("totalPage", ((total - 1) / pageSize) + 1);
        return dao.busca(page, pageSize);
    }

    /**
     *
     * @param bug
     */
    public void adiciona(final Bug bug)
    {
        if (bug.getTitulo() == null || bug.getTitulo().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(bug))
        {
            validator.add(new ValidationMessage("Essa bug já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario();

        dao.salva(bug);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     * @return
     */
    public Bug edita(Long id)
    {
        result.include("funcaoList", Arrays.asList(Funcao.values()));
        return dao.carrega(id);
    }

    /**
     *
     * @param bug
     */
    public void altera(Bug bug)
    {
        if (bug.getTitulo() == null || bug.getTitulo().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(bug))
        {
            validator.add(new ValidationMessage("Essa bug já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(bug.getId());

        dao.altera(bug);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     * @param id
     */
    public void remove(Long id)
    {
        Bug bug = dao.carrega(id);
        dao.remove(bug);
        result.redirectTo(this).lista(null,null);
    }

    /**
     *
     */
    public void formulario()
    {
         result.include("funcaoList", Arrays.asList(Funcao.values()));
    }
}
