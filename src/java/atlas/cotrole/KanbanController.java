package atlas.cotrole;

import atlas.dao.AlteracaoDAO;
import atlas.dao.SprintDAO;
import atlas.dao.TarefaDAO;
import atlas.modelo.Alteracao;
import atlas.modelo.Sprint;
import atlas.modelo.Tarefa;
import atlas.modelo.TipoAlteracao;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import br.com.caelum.vraptor.view.Results;
import java.util.Arrays;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class KanbanController
{
    private final AlteracaoDAO dao;
    private final SprintDAO sprintDAO;
    private final Result result;
    private final Validator validator;
    private final TarefaDAO tarefaDAO;

    /**
     *
     * @param alteracaoDAO
     * @param result
     * @param validator
     */
    public KanbanController(AlteracaoDAO alteracaoDAO, SprintDAO sprintDAO, TarefaDAO tarefaDAO, Result result, Validator validator)
    {
        this.tarefaDAO = tarefaDAO;
        this.validator = validator;
        this.result = result;
        this.dao = alteracaoDAO;
        this.sprintDAO = sprintDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/sprint/{sprint.id}/kanban")
    public void kanban(Sprint sprint)
    {
        result.include("sprint", sprintDAO.carrega(sprint.getId()));
    }
    
    @Path("/sprint/{sprint.id}/changeStatusTarefa")
    public void changeStatusTarefa(Tarefa tarefa)
    {
        Tarefa tarefaAlteraStatus = tarefaDAO.carrega(tarefa.getId());
        tarefaAlteraStatus.setStatus(tarefa.getStatus());
        tarefaAlteraStatus.setHistoria(tarefa.getHistoria());
        tarefaDAO.altera(tarefaAlteraStatus);
        
        result.use(Results.nothing());
    }
}
