package atlas.cotrole;

import atlas.dao.HistoriaDAO;
import atlas.dao.SprintDAO;
import atlas.modelo.Historia;
import atlas.modelo.Projeto;
import atlas.modelo.Sprint;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.ValidationMessage;
import java.util.List;

/**
 * @author Maison Chaves
 */
@Resource
public class HistoriaController
{
    private final HistoriaDAO dao;
    private final Result result;
    private final Validator validator;
    private final SprintDAO sprintDAO;

    /**
     *
     * @param historiaDAO
     * @param result
     * @param validator
     */
    public HistoriaController(SprintDAO sprintDAO, HistoriaDAO historiaDAO, Result result, Validator validator)
    {
        this.validator = validator;
        this.result = result;
        this.dao = historiaDAO;
        this.sprintDAO = sprintDAO;
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/projeto/{projeto.id}/historia/lista")
    public List<Historia> lista(Projeto projeto/*, Integer page, Integer pageSize*/)
    {
//        if (page == null || pageSize == null)
//        {
//            page = 1;
//            pageSize = 10;
//        }

        Historia historia = new Historia();
        historia.setProjeto(projeto);
        historia.setSprint(null);

        Integer total = dao.getTotalRegistros(historia);

        result.include("page", 1);
        result.include("pageSize", total);
        result.include("total", total);
        return dao.busca(historia);
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Path("/projeto/{projeto.id}/sprint/{sprint.id}/historia/lista")
    public List<Historia> lista(Sprint sprint, Projeto projeto/*, Integer page, Integer pageSize*/)
    {
//        if (page == null || pageSize == null)
//        {
//            page = 1;
//            pageSize = 10;
//        }

        Historia historia = new Historia();
        historia.setProjeto(projeto);
        historia.setSprint(sprint);

        Integer total = dao.getTotalRegistros(historia);

        result.include("page", 1);
        result.include("pageSize", total);
        result.include("total", total);
        return dao.busca(historia);
    }

    /**
     *
     * @param historia
     */
    @Path("/projeto/{projeto.id}/historia/adiciona")
    public void adiciona(Projeto projeto, final Historia historia)
    {
        if (historia.getTitulo() == null || historia.getTitulo().length() < 3)
        {
            validator.add(new ValidationMessage("Titulo é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(historia))
        {
            validator.add(new ValidationMessage("Essa historia já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).formulario(projeto);

        if (historia.getSprint().getId() == null)
        {
            historia.setSprint(null);
        }
        dao.salva(historia);
        result.redirectTo(this).lista(projeto);
    }

//    /**
//     *
//     * @param id
//     * @return
//     */
//    @Path("/projeto/{projeto.id}/sprint/{sprint.id}/historia/edita/{id}")
//    public void edita(Projeto projeto, Sprint sprint, Long id)
//    {
//        result.redirectTo(this).edita(projeto, id);
//    }
    
    /**
     *
     * @param id
     * @return
     */
    @Path({"/projeto/{projeto.id}/historia/edita/{id}", "/projeto/{projeto.id}/sprint/{sprint.id}/historia/edita/{id}"})
    public Historia edita(Projeto projeto, Long id)
    {
        Sprint sprint = new Sprint();
        sprint.setProjeto(projeto);
        result.include("sprintList", sprintDAO.busca(sprint));
        return dao.carrega(id);
    }

    /**
     *
     * @param historia
     */
    @Path({"/projeto/{projeto.id}/historia/altera", "/projeto/{projeto.id}/sprint/{sprint.id}/historia/altera"})
    public void altera(Projeto projeto, Historia historia)
    {
        if (historia.getTitulo() == null || historia.getTitulo().length() < 3)
        {
            validator.add(new ValidationMessage("Nome é obrigatório e precisa ter mais de 3 letras", "produto.nome"));
        }
        if (!dao.check(historia))
        {
            validator.add(new ValidationMessage("Essa historia já se encontra cadastrada", "produto.nome"));
        }
        validator.onErrorUsePageOf(this).edita(projeto, historia.getId());

        if (historia.getSprint().getId() == null)
        {
            historia.setSprint(null);
        }
        dao.altera(historia);
        result.redirectTo(this).lista(projeto);
    }

    /**
     *
     * @param id
     */
    @Path("/projeto/{projeto.id}/historia/remove/{id}")
    public void remove(Projeto projeto, Long id)
    {
        Historia historia = dao.carrega(id);
        dao.remove(historia);
        result.redirectTo(this).lista(projeto);
    }

    @Path("/projeto/{projeto.id}/historia/formulario")
    public Projeto formulario(Projeto projeto)
    {
        Sprint sprint = new Sprint();
        sprint.setProjeto(projeto);
        result.include("sprintList", sprintDAO.busca(sprint));
        return projeto;
    }
}
