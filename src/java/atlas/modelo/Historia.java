package atlas.modelo;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Historia implements Serializable
{
    private List<Tarefa> tarefas;
    private static final long serialVersionUID = 1L;
    private Long id;
    private String titulo;
    private String descricao;
    private Integer prioridade;
    private Integer pontos;
    private Sprint sprint;
    private Projeto projeto;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTitulo()
    {
        return titulo;
    }

    public void setTitulo(String titulo)
    {
        this.titulo = titulo;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public Integer getPrioridade()
    {
        return prioridade;
    }

    public void setPrioridade(Integer prioridade)
    {
        this.prioridade = prioridade;
    }

    public Integer getPontos()
    {
        return pontos;
    }

    public void setPontos(Integer pontos)
    {
        this.pontos = pontos;
    }

    @ManyToOne
    @JoinColumn(name = "sprintId")
    public Sprint getSprint()
    {
        return sprint;
    }

    public void setSprint(Sprint sprint)
    {
        this.sprint = sprint;
    }

    @ManyToOne
    @JoinColumn(name = "projetoId")
    public Projeto getProjeto()
    {
        return projeto;
    }

    public void setProjeto(Projeto projeto)
    {
        this.projeto = projeto;
    }

    @OneToMany(mappedBy = "historia")
    public List<Tarefa> getTarefas()
    {
        return tarefas;
    }

    public void setTarefas(List<Tarefa> tarefas)
    {
        this.tarefas = tarefas;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Historia))
        {
            return false;
        }
        Historia other = (Historia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Historias[ id=" + id + " ]";
    }

    @Transient
    public Integer getNumTarefas()
    {
        return this.tarefas == null ? 0 : this.tarefas.size();
    }

    @Transient
    public List<Alteracao> getAlteracoes()
    {
        List<Alteracao> listAlteracoes = new ArrayList<Alteracao>();
        if (tarefas != null)
        {
            for (Tarefa t : tarefas)
            {
                if (t.getAlteracoes() != null)
                {
                    listAlteracoes.addAll(t.getAlteracoes());
                }
            }
        }

        return listAlteracoes;
    }
}
