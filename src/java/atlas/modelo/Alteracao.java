
package atlas.modelo;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class Alteracao implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String observacao;
    private String referencia;
    private Boolean versionado;
    private Boolean maquinaTeste;
    private Boolean envioSite;
    @Enumerated(EnumType.STRING)
    private TipoAlteracao tipo;
    @ManyToOne
    @JoinColumn(name = "tarefaId")
    private Tarefa tarefa;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getReferencia()
    {
        return referencia;
    }

    public void setReferencia(String referencia)
    {
        this.referencia = referencia;
    }

    public Boolean getVersionado()
    {
        return versionado;
    }

    public void setVersionado(Boolean versionado)
    {
        this.versionado = versionado;
    }

    public Boolean getMaquinaTeste()
    {
        return maquinaTeste;
    }

    public void setMaquinaTeste(Boolean maquinaTeste)
    {
        this.maquinaTeste = maquinaTeste;
    }

    public TipoAlteracao getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoAlteracao tipo)
    {
        this.tipo = tipo;
    }

    public Tarefa getTarefa()
    {
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa)
    {
        this.tarefa = tarefa;
    }

    public String getObservacao()
    {
        return observacao;
    }

    public void setObservacao(String observacao)
    {
        this.observacao = observacao;
    }

    public Boolean getEnvioSite()
    {
        return envioSite;
    }

    public void setEnvioSite(Boolean envioSite)
    {
        this.envioSite = envioSite;
    }
    
    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alteracao))
        {
            return false;
        }
        Alteracao other = (Alteracao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.Alteracao[ id=" + id + " ]";
    }
    
}
