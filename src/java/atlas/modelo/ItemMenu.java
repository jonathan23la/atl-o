
package atlas.modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Maison Chaves
 */
@Entity
public class ItemMenu implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nome;
    private String link;
    @ManyToOne
    @JoinColumn(name = "paiId")
    private ItemMenu pai;
    @OneToMany(mappedBy = "pai")
    private List<ItemMenu> filhos;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }

    public String getLink()
    {
        return link;
    }

    public void setLink(String link)
    {
        this.link = link;
    }

    public ItemMenu getPai()
    {
        return pai;
    }

    public void setPai(ItemMenu pai)
    {
        this.pai = pai;
    }

    public List<ItemMenu> getFilhos()
    {
        return filhos;
    }

    public void setFilhos(List<ItemMenu> filhos)
    {
        this.filhos = filhos;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemMenu))
        {
            return false;
        }
        ItemMenu other = (ItemMenu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "atlas.modelo.ItemMenu[ id=" + id + " ]";
    }
    
}
