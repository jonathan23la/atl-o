
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum StatusTarefa
{
    N("Nova"), A("Em Andamento"), V("A Verificar"), F("Feito"), E("Entregue");
    private String descricao;

    private StatusTarefa(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
