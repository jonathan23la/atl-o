
package atlas.modelo;

/**
 *
 * @author Maison Chaves
 */
public enum TipoTarefa
{
    T("Tarefa"), I("Impedimento"), N("Não Planejada");
    private String descricao;

    private TipoTarefa(String descricao)
    {
        this.descricao = descricao;
    }

    public String getDescricao()
    {
        return descricao;
    }
}
