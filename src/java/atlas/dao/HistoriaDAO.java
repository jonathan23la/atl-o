package atlas.dao;

import atlas.modelo.Historia;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class HistoriaDAO extends AbstractBuscaDAO<Historia>
{
    /**
     *
     * @param session
     */
    public HistoriaDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param numero
     * @return
     */
    public List<Historia> busca(Integer numero)
    {
        return getSession().createCriteria(Historia.class).add(Restrictions.eq("numero", numero)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Historia> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Historia.class);

        criteria.addOrder(Order.desc("prioridade"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra historia como o mesmo nome excuindo ela
     * propria.
     *
     * @param historia
     * @return
     */
    public Boolean check(Historia historia)
    {
        Criteria check = getSession().createCriteria(Historia.class);
        if (historia != null)
        {
            if (historia.getTitulo() != null)
            {
                check.add(Restrictions.eq("titulo", historia.getTitulo()));
            }
            if (historia.getId() != null && historia.getId() != 0)
            {
                check.add(Restrictions.ne("id", historia.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Historia historia, Criteria criteria)
    {
        if (historia != null)
        {
            if (historia.getProjeto() != null && historia.getProjeto().getId() != 0)
            {
                criteria.add(Restrictions.eq("projeto", historia.getProjeto()));
            }
            if (historia.getSprint() != null && historia.getSprint().getId() != 0)
            {
                criteria.add(Restrictions.eq("sprint", historia.getSprint()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.desc("prioridade"));
    }
}
