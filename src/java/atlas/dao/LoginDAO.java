
package atlas.dao;

import atlas.infra.Criptografia;
import atlas.modelo.Login;
import br.com.caelum.vraptor.ioc.Component;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author André
 */
@Component
public class LoginDAO extends AbstractBuscaDAO<Login>
{
    private final Criptografia criptografia;

    /**
     *
     * @param session
     * @param criptografia
     */
    public LoginDAO(Session session, Criptografia criptografia)
    {
        super(session);
        this.criptografia = criptografia;
    }

    /**
     * Salva login e realiza criotografia da senha
     * @param login
     */
    @Override
    public Long salva(Login login)
    {
        try
        {
            Transaction tx = getSession().beginTransaction();
            String senha = criptografia.criptografa(login.getSenha());
            login.setSenha(senha);
            getSession().save(login);
            tx.commit();
        }
        catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Verifica a existencia de um login unico no site
     *
     * @param login 
     * @return Verdadeiro se existir o login informado
     */
    public Boolean checkLogin(Login login)
    {
        Criteria busca = getSession().createCriteria(Login.class);
        if (login != null)
        {
            if (login.getLogin() != null && !login.getLogin().equals(""))
            {
                busca.add(Restrictions.ilike("login", login.getLogin(), MatchMode.ANYWHERE));
            }
            if (login.getId()!= null && login.getId() != 0)
            {
                busca.add(Restrictions.ne("id", login.getId()));
            }
        }

        return !busca.list().isEmpty();
    }

    /**
     * Recupera do logins a partir do login e senha
     * @param login
     * @return
     */
    public Login carregaLogin(Login login)
    {
        getSession().clear();
        String senha;
        Login logado = null;
        try
        {
            senha = criptografia.criptografa(login.getSenha());
            logado = (Login) getSession().createCriteria(Login.class)
                .add(Restrictions.eq("login", login.getLogin()))
                .add(Restrictions.eq("senha", senha))
                .uniqueResult();
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.out.println("Erro cripotografia" + ex.getMessage());
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("Erro cripotografia" + ex.getMessage());
            Logger.getLogger(LoginDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logado;
    }

    @Override
    protected Criteria criteriosBusca(Login login, Criteria criteria)
    {
        if (login != null)
        {
            if (login.getLogin() != null && !login.getLogin().equals(""))
            {
                criteria.add(Restrictions.eq("login", login.getLogin()));
            }
        }
        return criteria;
    }
    
     @Override
    protected Criteria ordem(Criteria criteria) {
        return criteria.addOrder(Order.asc("login"));
    }

    public Boolean check(Login login) {
      
        Criteria check = getSession().createCriteria(Login.class);
        if (login != null)
        {
            if (login.getLogin() != null && !login.getLogin().equals(""))
            {
                check.add(Restrictions.eq("login", login.getLogin()));
            }
            if (login.getId()!= null && login.getId() != 0)
            {
                check.add(Restrictions.ne("id", login.getId()));
            }
        }

        return check.list().isEmpty();
    }
    
    /**
     * Salva o objeto na persistencia
     *
     * @param login
     * @throws HibernateException
     */
    public void salvaLogin(Login login) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(login);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                try {
                    tx.rollback();
                    System.out.println("Salva - " + getPersistentClass().getCanonicalName());
                    System.out.println("Erro " + e.getMessage());
                    System.out.println("Rollback");
                } catch (HibernateException he) {
                    System.out.println("Salva - " + getPersistentClass().getCanonicalName());
                    System.out.println("Erro " + he.getMessage());
                }
            }
        } 
    }
}
