package atlas.dao;

import atlas.modelo.Sprint;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class SprintDAO extends AbstractBuscaDAO<Sprint>
{
    /**
     *
     * @param session
     */
    public SprintDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param numero
     * @return
     */
    public List<Sprint> busca(Integer numero)
    {
        return getSession().createCriteria(Sprint.class).add(Restrictions.eq("numero", numero)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Sprint> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Sprint.class);

        criteria.addOrder(Order.asc("numero"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra sprint como o mesmo nome excuindo ela
     * propria.
     *
     * @param sprint
     * @return
     */
    public Boolean check(Sprint sprint)
    {
        Criteria check = getSession().createCriteria(Sprint.class);
        if (sprint != null)
        {
            if (sprint.getNumero()!= null)
            {
                check.add(Restrictions.eq("numero", sprint.getNumero()));
                check.add(Restrictions.eq("projeto", sprint.getProjeto()));
            }
            if (sprint.getId()!= null && sprint.getId() != 0)
            {
                check.add(Restrictions.ne("id", sprint.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Sprint sprint, Criteria criteria)
    {
        if (sprint != null)
        {
            if (sprint.getProjeto()!= null && sprint.getProjeto().getId()!= null)
            {
                criteria.add(Restrictions.eq("projeto", sprint.getProjeto()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("numero"));
    }
}
