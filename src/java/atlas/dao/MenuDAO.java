package atlas.dao;

import atlas.modelo.ItemMenu;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class MenuDAO extends AbstractDAO<ItemMenu>
{
    /**
     *
     * @param session
     */
    public MenuDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<ItemMenu> busca(String nome)
    {
        return getSession().createCriteria(ItemMenu.class).add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<ItemMenu> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(ItemMenu.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra menu como o mesmo nome excuindo ela
     * propria.
     *
     * @param itemMenu
     * @return
     */
    public Boolean check(ItemMenu itemMenu)
    {
        Criteria check = getSession().createCriteria(ItemMenu.class);
        if (itemMenu != null)
        {
            if (itemMenu.getNome() != null && !itemMenu.getNome().equals(""))
            {
                check.add(Restrictions.like("nome", itemMenu.getNome(), MatchMode.ANYWHERE));
            }
            if (itemMenu.getId()!= null && itemMenu.getId() != 0)
            {
                check.add(Restrictions.ne("id", itemMenu.getId()));
            }
        }

        return check.list().isEmpty();
    }
}
