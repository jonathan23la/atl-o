package atlas.dao;

import atlas.infra.Criptografia;
import atlas.modelo.Login;
import atlas.modelo.Membro;
import br.com.caelum.vraptor.ioc.Component;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class MembroDAO extends AbstractDAO<Membro>
{
    private final Criptografia criptografia;
    
    
    /**
     *
     * @param session
     */
    public MembroDAO(Session session, Criptografia criptografia)
    {
        super(session);
        this.criptografia = criptografia;
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<Membro> busca(String nome)
    {
        return getSession().createCriteria(Membro.class).add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Membro> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Membro.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra membro como o mesmo nome excuindo ela
     * propria.
     *
     * @param membro
     * @return
     */
    public Boolean check(Membro membro)
    {
        Criteria check = getSession().createCriteria(Membro.class);
        if (membro != null)
        {
            if (membro.getNome() != null && !membro.getNome().equals(""))
            {
                check.add(Restrictions.like("nome", membro.getNome(), MatchMode.ANYWHERE));
            }
            if (membro.getId()!= null && membro.getId() != 0)
            {
                check.add(Restrictions.ne("id", membro.getId()));
            }
        }

        return check.list().isEmpty();
    }

    public Membro carregaLogin(Login login) {
        getSession().clear();
        String senha;
        Membro logado = null;
        try
        {
            senha = criptografia.criptografa(login.getSenha());
            logado = (Membro) getSession().createCriteria(Membro.class).createCriteria("login")
                .add(Restrictions.eq("login", login.getLogin()))
                .add(Restrictions.eq("senha", senha))
                .uniqueResult();
        }
        catch (NoSuchAlgorithmException ex)
        {
            System.out.println("Erro cripotografia" + ex.getMessage());
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            System.out.println("Erro cripotografia" + ex.getMessage());
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logado;
    }
    
    /**
     * Salva usuario e realiza criotografia da senha
     * @param membro
     */
    @Override
    public Long salva(Membro membro)
    {
        try
        {
            Transaction tx = getSession().beginTransaction();
            String senha = criptografia.criptografa(membro.getLogin().getSenha());
            membro.getLogin().setSenha(senha);
            getSession().save(membro);
            tx.commit();
        }
        catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void alteraSenha(Login login) {
        try
        {
            Transaction tx = getSession().beginTransaction();
            String senha = criptografia.criptografa(login.getSenha());
            login.setSenha(senha);
            getSession().update(login);
            tx.commit();
        }
        catch (NoSuchAlgorithmException ex)
        {
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            Logger.getLogger(MembroDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
