package atlas.dao;

import atlas.modelo.Projeto;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class ProjetoDAO extends AbstractDAO<Projeto>
{
    /**
     *
     * @param session
     */
    public ProjetoDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<Projeto> busca(String nome)
    {
        return getSession().createCriteria(Projeto.class).add(Restrictions.ilike("nome", nome, MatchMode.ANYWHERE)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Projeto> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Projeto.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra projeto como o mesmo nome excuindo ela
     * propria.
     *
     * @param projeto
     * @return
     */
    public Boolean check(Projeto projeto)
    {
        Criteria check = getSession().createCriteria(Projeto.class);
        if (projeto != null)
        {
            if (projeto.getNome() != null && !projeto.getNome().equals(""))
            {
                check.add(Restrictions.like("nome", projeto.getNome(), MatchMode.ANYWHERE));
            }
            if (projeto.getId()!= null && projeto.getId() != 0)
            {
                check.add(Restrictions.ne("id", projeto.getId()));
            }
        }

        return check.list().isEmpty();
    }
}
