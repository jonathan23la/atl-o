package atlas.dao;

import atlas.modelo.Elemento;
import br.com.caelum.vraptor.ioc.Component;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Clase de teste para uma DAO extendido apartir de uma DAO generico
 *
 * @author Maison Chaves
 */
@Component
public class ElementoDAO extends AbstractBuscaDAO<Elemento>
{
    /**
     *
     * @param session
     */
    public ElementoDAO(Session session)
    {
        super(session);
    }

    /**
     *
     * @param nome
     * @return
     */
    public List<Elemento> busca(String nome)
    {
        return getSession().createCriteria(Elemento.class).add(Restrictions.eq("nome", nome)).list();
    }

    /**
     *
     * @param page
     * @param pageSize
     * @return
     */
    public List<Elemento> busca(Integer page, Integer pageSize)
    {
        Criteria criteria = getSession().createCriteria(Elemento.class);

        criteria.addOrder(Order.asc("nome"));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
        return criteria.list();
    }

    /**
     * Checa se não existe outra elemento como o mesmo nome excuindo ela
     * propria.
     *
     * @param elemento
     * @return
     */
    public Boolean check(Elemento elemento)
    {
        Criteria check = getSession().createCriteria(Elemento.class);
        if (elemento != null)
        {
            if (elemento.getNome()!= null)
            {
                check.add(Restrictions.eq("nome", elemento.getNome()));
                check.add(Restrictions.eq("diretorio", elemento.getDiretorio()));
            }
            if (elemento.getId()!= null && elemento.getId() != 0)
            {
                check.add(Restrictions.ne("id", elemento.getId()));
            }
        }

        return check.list().isEmpty();
    }

    @Override
    protected Criteria criteriosBusca(Elemento elemento, Criteria criteria)
    {
        if (elemento != null)
        {
            if (elemento.getNome()!= null && !elemento.getNome().equals(""))
            {
                criteria.add(Restrictions.eq("nome", elemento.getNome()));
            }
        }
        return criteria;
    }

    @Override
    protected Criteria ordem(Criteria criteria)
    {
        return criteria.addOrder(Order.asc("nome"));
    }
}
