
package atlas.modelo;

/**
 *
 * @author Maison
 */
import java.io.IOException;
import org.apache.log4j.Appender;
import org.apache.log4j.Logger;  
import org.apache.log4j.Level;  
import org.apache.log4j.ConsoleAppender;
  
public class LoggingTest {  
    static Logger logger = Logger.getLogger(LoggingTest.class);  
  
    public static void main(String[] args) throws IOException {  
        //BasicConfigurator.configure(); 
        
        //Appender fileAppender = new FileAppender(new LayoutGCI(), "myLogFile.log");
        Appender consoleAppender = new ConsoleAppender(new LayoutGCI());
        
        //logger.addAppender(fileAppender);
        logger.addAppender(consoleAppender);
  
        logger.setLevel(Level.INFO);  
        logger.debug("Isso nao vai aparecer...");  
        logger.info("Inicializando...");  
          
        try {  
            throw new Exception("Loga esse, Log4J!");  
        } catch (Exception e) {  
            logger.error("Oops, deu erro: " + e.getMessage());  
        }  
  
        logger.info("Finalizando...");  
    }     
} 
