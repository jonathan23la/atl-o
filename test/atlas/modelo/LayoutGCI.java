
package atlas.modelo;

import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * @author Maison
 */
public class LayoutGCI extends Layout{

    @Override
    public String format(LoggingEvent le) {
        return le.getLevel().toString() + "\n";
    }

    @Override
    public boolean ignoresThrowable() {
        return true;
    }

    @Override
    public void activateOptions() {
    }
    
}
