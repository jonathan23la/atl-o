
package atlas.modelo;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Maison
 */
public class ControleAlteracaoTest
{
    /**
     * Test of getId method, of class Erro.
     */
    @Test
    public void sprintGetAlteracoes()
    {
        Sprint sprint = new Sprint();
        List<Alteracao> listaAlteracoes = sprint.getAlteracoes();
        
        assertEquals(0, listaAlteracoes.size());
    }
    
    @Test
    public void historiaGetAlteracoes()
    {
        Alteracao a1 = new Alteracao();
        Alteracao a2 = new Alteracao();
        Alteracao a3 = new Alteracao();
        Alteracao a4 = new Alteracao();
        
        Tarefa t1 = new Tarefa();
        Tarefa t2 = new Tarefa();
        
        ArrayList<Alteracao> alist1 = new ArrayList<Alteracao>();
        alist1.add(a1);
        alist1.add(a2);
        
        t1.setAlteracoes(alist1);
        
        ArrayList<Alteracao> alist2 = new ArrayList<Alteracao>();
        alist1.add(a3);
        alist1.add(a4);
        
        t2.setAlteracoes(alist2);
        
        Historia historia = new Historia();
        
        List<Tarefa> tList = new ArrayList<Tarefa>();
        tList.add(t1);
        tList.add(t2);
        
        historia.setTarefas(tList);
        
        List<Alteracao> expecteds = new ArrayList<Alteracao>();
        expecteds.add(a1);
        expecteds.add(a2);
        expecteds.add(a3);
        expecteds.add(a4);
        List<Alteracao> listaAlteracoes = historia.getAlteracoes();
        
        assertEquals(expecteds.size(), listaAlteracoes.size());
        
    }
    
    @Test
    public void historiaGetAlteracoesComListaVazia()
    {
        Historia historia = new Historia();
        
        List<Alteracao> listaAlteracoes = historia.getAlteracoes();
        
        assertEquals(0, listaAlteracoes.size());
    }
}